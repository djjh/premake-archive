-- filesystem.lua
-- The filesystem Premake Module.

local filesystem = require "filesystem"

local m = {}

m._VERSION = "0.0.0"

local exe_tar = filesystem.find("TAR", "tar", "tar.exe")
local exe_7z = filesystem.find("SEVENZIP", "7za", "7za.exe", "7z", "7z.exe")
local exe_unzip = filesystem.find("unzip", "unzip.exe")

function m.extract_archive(archive_path, output_path, error_fun)
    local dst = output_path or os.getcwd()
    error_fun = error_fun or function() end
    if os.isfile(archive_path) then
        if string.contains(archive_path, ".tar.gz") then
            if exe_7z then
                return os.executef([[%s x "%s" -so | %s x -aoa -bso0 -si -ttar -o"%s"]], exe_7z, archive_path, exe_7z, dst)
            elseif exe_tar then
                return os.executef([[%s -xzf "%s" -C "%s"]], exe_tar, archive_path, dst)
            else
                error_fun("Unable to extract archive. No archive tool found.")
            end
        elseif string.contains(archive_path, ".zip") then
            if exe_unzip then
                return os.executef([[%s -o -q "%s" -d "%s"]], exe_unzip, archive_path, dst)
            elseif exe_7z then
                return os.executef([[%s x "%s" -aoa -bso0 -tzip -o"%s"]], exe_7z, archive_path, dst)
            else
                error_fun("Unable to extract archive. No archive tool found.")
            end
        else
            error_fun("Unable to extract archive. Unsupported type.")
        end
    else
        error_fun("Unable to extract archive. File not found.")
    end
end

function m.create_archive(paths, name)

    if string.contains(name, ".tar.gz") or string.contains(name, ".tgz") then
        name = string.match(name, "[%w_%-]*")
    end

    local tarname = name .. ".tar"
    local gzname = name .. ".tgz"

    local files = {}
    for _,v in ipairs(paths) do
        if os.isfile(v) or os.isdir(v) then
            table.insert(files, v)
        end
    end

    local file_list = table.implode(files, [["]], [["]], [[ ]])

    if exe_7z then
        local cmd = string.format([[%s a -ttar "%s" %s]], exe_7z, tarname, file_list)
        assert_return_code(os.execute(cmd))
        cmd = string.format([[%s a -tgzip "%s" "%s"]], exe_7z, gzname, tarname)
        return assert_return_code(os.execute(cmd))
    elseif exe_tar then
        local cmd = string.format([[%s -czf "%s" %s]], exe_tar, gzname, file_list)
        return assert_return_code(os.execute(cmd))
    end
end

return m
